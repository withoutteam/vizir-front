import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import { RECEBE_TABELA_PRECO } from "./mutationTypes";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    tabelaPrecos: []
  },

  getters: {
    getTabelaPrecos: state => state.tabelaPrecos
  },

  actions: {
    async actionConsultaTabelaPrecos({ commit }) {
      return axios
        .get("http://localhost:3333/tabela-preco/")
        .then(res => commit(RECEBE_TABELA_PRECO, res.data.conteudo))
        .catch(err => console.warn(err));
    }
  },

  mutations: {
    [RECEBE_TABELA_PRECO](state, listaTabelaPrecos) {
      state.tabelaPrecos = listaTabelaPrecos;
    }
  }
});
