# Front End - Telzir

Aplicação SPA desenvolvida com ***VueJs*** para ser utilizada junto à API Telzir. Criada para o processo seletivo da Vizir.

## Execução do projeto:

Para rodar a aplicação, utilize os comandos `npm run serve` ou `yarn serve` para rodar em modo desenvolvimento. Para execução do mesmo em produção, realize primeiramente o build para o processo de minimificação do Javascript, caso não existir, será criada uma pasta **dist** na raiz do projeto, os arquivos contidos nessa pasta devem ser executados em um servidor HTTP.